# Download
As the following scripts use random numbers, the simulation results might differ (e.g. when using python2.7 instead of python3*).
Therefore it is recommended to use the archived version on Zenodo to reproduce the results (https://zenodo.org/record/1346307).


# Re-Simulation
In case you want to rerun the simulation follow this guide:

### Dependencies
For a rerun of the simulation the following programs are needed:
* snakemake (3.15)
* NEAT (v0.2, commit 5b8bc84ddd3b7bd32b425cb5dd5245bf854148db)
* python3.4 or higher (for snakemake)
* python2.7 or conda (for NEAT) 
* python module numpy 1.9.1+ (for NEAT)
* tabix (0.2.5) 
* vcftools (v0.1.12)
* R (3.3)

### Required in working directory
* genomes (untar genomes.tar.gz)
* genome_info.txt
* all_coverage0.bed
* envs

### Commands

Simulate SNPs:  
```
Rscript simulation_snps.R
```

Simulate SNPs:   
(change paths to genomes in script!)  
(all_coverage0.bed is needed, change path or make sure it is located in working directory)  
```
Rscript simulation_coverage.R
```

Create vcfs with SNPs and deletions for each sample, using the data generated with R:  
```
bedtools sort -i simulation_dataset_coverageinfo_intervalsnocoverage.bed > simulation_dataset_coverageinfo_intervalsnocoverage_sorted.bed
bedtools merge -i simulation_dataset_coverageinfo_intervalsnocoverage_sorted.bed > simulation_dataset_coverageinfo_intervalsnocoverage_merged.bed
python3 vcf_for_simulation.py --input_smp simulation_dataset_sampleinfo.txt --input_snp simulation_dataset_snpinfo.txt --input_coverage simulation_dataset_coverageinfo_intervalsnocoverage_merged.bed
```



For each genome (g1, g2, g3, g4) simulate reads with NEAT with the provided Snakefile, specifying the genome in snakemake's config:  
```
snakemake -s simulation_pipeline.Snakefile --use-conda --config genome_info=genome_info.txt genome=g1
```