import glob
GENOME_INFO=config["genome_info"]

GENOME_DICT = dict()
with open(GENOME_INFO, "r") as inf:
    for line in inf:
        fields = line.strip().split("\t")
        GENOME_DICT[fields[0]] = fields[1]

# genome name e.g. g1
GENOME = config.get("genome")
        
SAMPLEFILES = glob.glob("coverage_vcfs/" + GENOME +  "/*_deletions.vcf")
SAMPLES = [os.path.splitext(os.path.basename(s))[0].rsplit("_",1)[0] for s in SAMPLEFILES]    


NEAT_PATH="~/software/neat-genreads/genReads.py"
VCFTOOLS_PATH="/usr/bin"
COVERAGE=50
READ_LENGTH=100
FRAG_LENGTH=300
FRAG_SD = 30

def calc_seed(wildcards):
    # smp: e.g. "simulation_C20_S312"
    fields = wildcards["smp"].split("_")
    c = int(fields[1][1:])
    s = int(fields[2][1:])
    return (c*s)

def get_genome_f(wildcards):
    return GENOME_DICT[wildcards["genome"]]
    
rule all:
    input:
        expand("simulation_vcfs/{genome}/{smp}.vcf", smp = SAMPLES, genome=GENOME),
        expand("simulation/{genome}/{smp}_read1.fastq.gz", smp = SAMPLES, genome=GENOME),
        expand("simulation/{genome}/{smp}_read2.fastq.gz", smp = SAMPLES, genome=GENOME),
    

rule filtersnps:
    input:
        avcf="{vcffile}_allele_sorted.vcf.gz",
        aidx="{vcffile}_allele_sorted.vcf.gz.tbi",
        dvcf="{vcffile}_deletions.vcf.gz",
        didx="{vcffile}_deletions.vcf.gz.tbi"    
    output:
        "{vcffile}_filtered.vcf"
    shell:
        "{VCFTOOLS_PATH}/vcf-isec -c {input.avcf} {input.dvcf} > {output}"

rule tabix:
    input:
        "{vcffile}.vcf"
    output:
        gz="{vcffile}.vcf.gz",
        tbi="{vcffile}.vcf.gz.tbi"
    shell:
        """
        bgzip -c {input} > {output.gz}
        tabix {output.gz}
        """  

rule sort:
    input:
        "{vcffile}.vcf"
    output:
        "{vcffile}_sorted.vcf"
    shell:
        "{VCFTOOLS_PATH}/vcf-sort {input} > {output}"

        
rule combine_vcf:
    input: 
        "coverage_vcfs/{genome}/{smp}_filtered.vcf.gz",
        "coverage_vcfs/{genome}/{smp}_deletions.vcf.gz"
    output:
        "simulation_vcfs/{genome}/{smp}.vcf"
    shell:
        "{VCFTOOLS_PATH}/vcf-concat {input} > {output}"


rule neat:
    input:
        vcf="simulation_vcfs/{genome}/{smp}.vcf",
        genome=get_genome_f
    output:
        "simulation/{genome}/{smp}_read1.fq",
        "simulation/{genome}/{smp}_read2.fq"
    params:
        out="simulation/{genome}/{smp}", seed=calc_seed
    log:
        "simulation_logs/{genome}/{smp}_log.txt"
    conda:
        "envs/py27.yml"
    shell:
        "python {NEAT_PATH} --rng {params.seed} -M 0 -p 2 -r {input.genome} -R {READ_LENGTH} -o {params.out} -v {input.vcf} -c {COVERAGE} --pe {FRAG_LENGTH} {FRAG_SD} > {log}"
    

rule gz:
    input:
        "{file}.fq"
    output:
        "{file}.fastq.gz"
    shell:
        "gzip -c {input} > {output}"
